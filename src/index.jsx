import { h, render, Component } from 'preact';

import { Main } from './pages/main';

document.addEventListener("DOMContentLoaded", () => {
  render(<Main />, document.getElementsByTagName('app')[0]);
});