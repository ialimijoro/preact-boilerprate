const path = require("path");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

var isProd = process.env.NODE_ENV === "production";

var minimizer_arr = isProd ? [new UglifyJsPlugin()] : [];

module.exports = {
  entry: "./src/index.jsx",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.bundle.js",
    publicPath: "/dist/"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        resolve: {
          extensions: [".js", ".jsx"]
        },
        use: ["babel-loader"]
      }
    ]
  },
  optimization: {
    minimizer: minimizer_arr
  }
};
